package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	
	private Ingredient[] composition;
	private long id;
	private String name;

	  public Pizza() {
	  }

	  public Pizza(long id, String name, Ingredient[] compo) {
	    this.id = id;
	    this.name = name;
	    this.composition = compo;
	  }
	  
	  public Pizza(long id, String name) {
		  this.id = id;
		  this.name = name;
	  }

	  public void setId(long id) {
	    this.id = id;
	  }

	  public long getId() {
	    return id;
	  }

	  public String getName() {
	    return name;
	  }

	  public void setName(String name) {
	    this.name = name;
	  }
	  
	  public Ingredient[] getComposition() {
			return composition;
		}

		public void setComposition(Ingredient[] composition) {
			this.composition = composition;
		}


	  public static PizzaDto toDto(Pizza p) {
	    PizzaDto dto = new PizzaDto();
	    dto.setId(p.getId());
	    dto.setName(p.getName());
	    dto.setComposition(p.getComposition());

	    return dto;
	  }

	
	public static Pizza fromDto(PizzaDto dto) {
	    Pizza pizza = new Pizza();
	    pizza.setId(dto.getId());
	    pizza.setName(dto.getName());
	    pizza.setComposition(dto.getComposition());

	    return pizza;
	  }
	  
	  @Override
	  public boolean equals(Object obj) {
	    if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    Pizza other = (Pizza) obj;
	    if (id != other.id)
	        return false;
	    if (name == null) {
	        if (other.name != null)
	            return false;
	    } else if (!name.equals(other.name))
	        return false;
	    return true;
	  }

	  @Override
	  public String toString() {
	    return "Pizza [id=" + id + ", name=" + name + ", ingredients=" + composition.toString() + "]";
	  }
	  
		
	  public static PizzaCreateDto toCreateDto(Pizza pizza) {
		  PizzaCreateDto dto = new PizzaCreateDto();
		  dto.setName(pizza.getName());

		  return dto;
	  }

	  public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
		  Pizza pizza = new Pizza();
		  pizza.setName(dto.getName());

		  return pizza;
	  }
	

}
