package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
	void createPizzaTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzaIngredientAssociation (pizza INTEGER NOT NULL, ingredient INTEGER NOT NULL, PRIMARY KEY(pizza, ingredient), FOREIGN KEY(pizza) REFERENCES pizzas(id), FOREIGN KEY(ingredient) REFERENCES composition(id))")
	void createAssociationTable();

	@SqlUpdate("DROP TABLE IF EXISTS pizzas; DROP TABLE IF EXISTS pizzaIngredientAssociation")
	void dropTable();

	@SqlUpdate("INSERT INTO Pizzas (name) VALUES (:name)")
	@GetGeneratedKeys
	long insert(String name);

	@SqlQuery("SELECT * FROM Pizzas")
	@RegisterBeanMapper(Pizza.class)
	List<Pizza> getAll();

	@SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
	@RegisterBeanMapper(Pizza.class)
	Pizza findById(long id);

	@SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByName(String name);

	@SqlUpdate("DELETE FROM Pizzas WHERE id = :id")
	void remove(long id);
	
	@SqlUpdate("INSERT INTO pizzaIngredientAssociation (idPizza, idIngredient) VALUES (:pizza, :ingredient)")
	@GetGeneratedKeys
	long insertIngredient(long idPizza, long idIngredient);

	@SqlQuery("SELECT * FROM pizzaIngredientAssociation WHERE pizza = :pizza AND ingredient = :ingredient")
	@RegisterBeanMapper(Pizza.class)
	Pizza findAssociationByIds(long idPizza, long idIngredient);


	@Transaction
	default void createTableAndPizzaAssociation() {
		createAssociationTable();
		createPizzaTable();
	}
}
