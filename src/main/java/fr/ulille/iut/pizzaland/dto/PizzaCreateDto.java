package fr.ulille.iut.pizzaland.dto;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaCreateDto {
	private String name;
	private Ingredient[] composition;

	public PizzaCreateDto() {}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public Ingredient[] getListeIngredient() {
		return composition;
	}

	public void setListeIngredient(Ingredient[] compo) {
		this.composition = compo;
	}
}
